package victor_gonzalez_ollervidez.kinedu.utils

import androidx.lifecycle.ViewModel
import victor_gonzalez_ollervidez.kinedu.networking.KineduRepository

open class BaseViewModel: ViewModel(){
    val repository = KineduRepository()
}