package victor_gonzalez_ollervidez.kinedu.utils;

import android.view.View;

public interface Interfaces {

    interface OnClickListener<T>{
        void onClick(T t, int pos, View v);
    }

}
