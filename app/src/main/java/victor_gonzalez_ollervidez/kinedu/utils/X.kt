package victor_gonzalez_ollervidez.kinedu.utils

import android.app.Activity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.BuildConfig
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.snackbar.Snackbar


fun Fragment.toast(msg: String, duration:Int = Toast.LENGTH_SHORT){
    Toast.makeText(context,msg,duration).show()
}

fun Activity.showSnackBar(
    v : View,
    msg : String,
    duration : Int = Snackbar.LENGTH_SHORT,
    actionMsg: String,
    action: () -> Unit ){
    //Toast.makeText(context,msg,duration).show()
    Snackbar.make(v,msg, duration).setAction(actionMsg) {
        action()
    }.show()
}

fun Fragment.showSnackBar(
    v : View,
    msg : String,
    duration : Int = Snackbar.LENGTH_SHORT,
    actionMsg: String,
    action: () -> Unit ){
    //Toast.makeText(context,msg,duration).show()
    Snackbar.make(v,msg, duration).setAction(actionMsg) {
        action()
    }.show()
}

fun ImageView.load(url: String){
    Glide
        .with(context)
        .load(url)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

fun Any.log(msg: Any, TAG: String = javaClass.simpleName){
    if(BuildConfig.DEBUG){
        Log.v(TAG, msg.toString())
    }
}