package victor_gonzalez_ollervidez.kinedu.utils.pojos.Articles

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("articles")
	val articles: List<Article>? = null
)