package victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("related_items")
	val relatedItems: RelatedItems? = null,

	@field:SerializedName("article")
	val article: Article? = null
)