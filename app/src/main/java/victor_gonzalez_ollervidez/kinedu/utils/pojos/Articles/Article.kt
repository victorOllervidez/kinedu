package victor_gonzalez_ollervidez.kinedu.utils.pojos.Articles

import com.google.gson.annotations.SerializedName

data class Article(

	@field:SerializedName("type")
	val type: String,

	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("name")
	val name: String,

	@field:SerializedName("min_age")
	val min_age: Int? = null,

	@field:SerializedName("max_age")
	val max_age: Int? = null,

	@field:SerializedName("picture")
	val picture: String? = null,

	@field:SerializedName("area_id")
	val areaId: Int? = null,

	@field:SerializedName("short_description")
	val shortDescription: String




)