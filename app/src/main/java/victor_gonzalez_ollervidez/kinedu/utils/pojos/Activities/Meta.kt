package victor_gonzalez_ollervidez.kinedu.utils.pojos.Activities

import com.google.gson.annotations.SerializedName

data class Meta(

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("page")
	val page: Int? = null,

	@field:SerializedName("total_pages")
	val totalPages: Int? = null
)