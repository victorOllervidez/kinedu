package victor_gonzalez_ollervidez.kinedu.utils.pojos.Activities

import com.google.gson.annotations.SerializedName

data class Activity(

	@field:SerializedName("domain_id")
	val domainId: Int? = null,

	@field:SerializedName("is_holiday")
	val isHoliday: Boolean? = null,

	@field:SerializedName("thumbnail")
	val thumbnail: String? = null,

	@field:SerializedName("purpose")
	val purpose: String? = null,

	@field:SerializedName("completed_milestones")
	val completedMilestones: Int? = null,

	@field:SerializedName("age_group")
	val ageGroup: String? = null,

	@field:SerializedName("activity_type")
	val activityType: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("area_id")
	val areaId: Int? = null,

	@field:SerializedName("age")
	val age: Int? = null,

	@field:SerializedName("active_milestones")
	val activeMilestones: Int? = null
)