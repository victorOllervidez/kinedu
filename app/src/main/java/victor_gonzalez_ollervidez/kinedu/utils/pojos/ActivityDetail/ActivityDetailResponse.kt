package victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail

import com.google.gson.annotations.SerializedName

data class ActivityDetailResponse(

	@field:SerializedName("data")
	val data: Data? = null
)