package victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("activity")
	val activity: ActivityDetail
)