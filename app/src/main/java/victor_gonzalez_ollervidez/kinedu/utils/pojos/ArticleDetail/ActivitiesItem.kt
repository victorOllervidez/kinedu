package victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail

import com.google.gson.annotations.SerializedName

data class ActivitiesItem(

	@field:SerializedName("purpose")
	val purpose: String? = null,

	@field:SerializedName("age_group")
	val ageGroup: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("video_provider_thumbnails")
	val videoProviderThumbnails: VideoProviderThumbnails? = null,

	@field:SerializedName("area_id")
	val areaId: Int? = null,

	@field:SerializedName("shareable_video_url")
	val shareableVideoUrl: String? = null,

	@field:SerializedName("picture")
	val picture: String? = null,

	@field:SerializedName("faved")
	val faved: Boolean? = null,

	@field:SerializedName("activity_type")
	val activityType: String? = null,

	@field:SerializedName("shareable_thumbnail_url")
	val shareableThumbnailUrl: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("locked")
	val locked: Boolean? = null,

	@field:SerializedName("age")
	val age: Int? = null,

	@field:SerializedName("video_id")
	val videoId: String? = null
)