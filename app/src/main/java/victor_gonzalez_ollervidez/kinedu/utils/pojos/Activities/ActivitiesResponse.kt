package victor_gonzalez_ollervidez.kinedu.utils.pojos.Activities

import com.google.gson.annotations.SerializedName

data class ActivitiesResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)