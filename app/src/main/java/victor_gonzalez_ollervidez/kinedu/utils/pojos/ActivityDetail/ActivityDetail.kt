package victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail

import com.google.gson.annotations.SerializedName

data class ActivityDetail(

	@field:SerializedName("embed_code")
	val embedCode: String? = null,

	@field:SerializedName("materials_list")
	val materialsList: List<MaterialsListItem?>? = null,

	@field:SerializedName("purpose")
	val purpose: String? = null,

	@field:SerializedName("age_group")
	val ageGroup: String? = null,

	@field:SerializedName("rating")
	val rating: Any? = null,

	@field:SerializedName("materials_description")
	val materialsDescription: Any? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("video_provider_thumbnails")
	val videoProviderThumbnails: VideoProviderThumbnails? = null,

	@field:SerializedName("area_id")
	val areaId: Int? = null,

	@field:SerializedName("shareable_video_url")
	val shareableVideoUrl: String? = null,

	@field:SerializedName("likes_count")
	val likesCount: Int? = null,

	@field:SerializedName("faved")
	val faved: Boolean? = null,

	@field:SerializedName("activity_type")
	val activityType: String? = null,

	@field:SerializedName("shareable_thumbnail_url")
	val shareableThumbnailUrl: String? = null,

	@field:SerializedName("overall_rating_avg")
	val overallRatingAvg: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("video_urls")
	val videoUrls: VideoUrls? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("milestones")
	val milestones: List<MilestonesItem?>? = null,

	@field:SerializedName("age")
	val age: Int? = null,

	@field:SerializedName("video_id")
	val videoId: String? = null
)