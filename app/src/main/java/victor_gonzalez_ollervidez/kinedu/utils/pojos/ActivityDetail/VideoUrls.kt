package victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail

import com.google.gson.annotations.SerializedName

data class VideoUrls(

	@field:SerializedName("size3")
	val size3: String? = null,

	@field:SerializedName("size1")
	val size1: String? = null,

	@field:SerializedName("size2")
	val size2: String? = null
)