package victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail

import com.google.gson.annotations.SerializedName

data class ArticleDetailResponse(

	@field:SerializedName("data")
	val data: Data? = null
)