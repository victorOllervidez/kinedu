package victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail

import com.google.gson.annotations.SerializedName

data class VideoProviderThumbnails(

	@field:SerializedName("size5")
	val size5: String? = null,

	@field:SerializedName("size6")
	val size6: String? = null,

	@field:SerializedName("size3")
	val size3: String? = null,

	@field:SerializedName("size4")
	val size4: String? = null,

	@field:SerializedName("size1")
	val size1: String? = null,

	@field:SerializedName("size2")
	val size2: String? = null
)