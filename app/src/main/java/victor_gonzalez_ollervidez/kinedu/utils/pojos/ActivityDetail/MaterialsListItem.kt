package victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail

import com.google.gson.annotations.SerializedName

data class MaterialsListItem(

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)