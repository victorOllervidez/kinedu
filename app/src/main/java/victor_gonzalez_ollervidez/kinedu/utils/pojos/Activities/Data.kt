package victor_gonzalez_ollervidez.kinedu.utils.pojos.Activities

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("activities")
	val activities: List<Activity>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("type")
	val type: String? = null
)