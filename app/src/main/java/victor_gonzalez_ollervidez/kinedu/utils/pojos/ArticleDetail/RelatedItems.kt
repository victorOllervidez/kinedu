package victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail

import com.google.gson.annotations.SerializedName

data class RelatedItems(

	@field:SerializedName("activities")
	val activities: List<ActivitiesItem?>? = null,

	@field:SerializedName("articles")
	val articles: List<ArticlesItem?>? = null
)