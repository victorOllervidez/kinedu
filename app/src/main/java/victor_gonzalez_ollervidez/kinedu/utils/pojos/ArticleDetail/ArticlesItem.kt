package victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail

import com.google.gson.annotations.SerializedName

data class ArticlesItem(

	@field:SerializedName("short_description")
	val shortDescription: String? = null,

	@field:SerializedName("faved")
	val faved: Boolean? = null,

	@field:SerializedName("link")
	val link: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("area_id")
	val areaId: Int? = null,

	@field:SerializedName("picture")
	val picture: String? = null
)