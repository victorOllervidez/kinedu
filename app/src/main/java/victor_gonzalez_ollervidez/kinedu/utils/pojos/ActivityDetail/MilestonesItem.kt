package victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail

import com.google.gson.annotations.SerializedName

data class MilestonesItem(

	@field:SerializedName("answer")
	val answer: Any? = null,

	@field:SerializedName("age_group")
	val ageGroup: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("skill_id")
	val skillId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("area_id")
	val areaId: Int? = null,

	@field:SerializedName("age")
	val age: Int? = null
)