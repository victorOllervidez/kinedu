package victor_gonzalez_ollervidez.kinedu.utils.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.find
import victor_gonzalez_ollervidez.kinedu.R
import victor_gonzalez_ollervidez.kinedu.utils.AspectRatioImageView
import victor_gonzalez_ollervidez.kinedu.utils.Interfaces
import victor_gonzalez_ollervidez.kinedu.utils.load
import victor_gonzalez_ollervidez.kinedu.utils.pojos.Articles.Article

class ArticlesListAdapter: RecyclerView.Adapter<ArticlesListAdapter.ArticleHolder>() {

    val TAG = javaClass.simpleName

    lateinit var onClickListener: Interfaces.OnClickListener<Article>

    private val articlesList = ArrayList<Article>()

    fun bindList(articles: List<Article>){
        this.articlesList.addAll(articles)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ArticleHolder {
        return ArticleHolder(
            LayoutInflater.from(
                parent.context
            ).inflate(R.layout.article_list_item, parent, false)
        )
    }

    override fun getItemCount() = this.articlesList.size

    override fun onBindViewHolder(holder: ArticleHolder, position: Int) {
        holder.bindItem(
            articlesList[position]
        )
    }


    inner class ArticleHolder (itemView: View) : RecyclerView.ViewHolder(itemView){

        private val articleImage = itemView.find<AspectRatioImageView>(R.id.article_image)
        private val articleTitle = itemView.find<TextView>(R.id.article_title)
        private val articleBody = itemView.find<TextView>(R.id.article_body)

        init {
            itemView.setOnClickListener {
                if(::onClickListener.isInitialized){
                    onClickListener.onClick(
                        articlesList[adapterPosition],
                        adapterPosition,
                        itemView
                    )
                }
            }
        }

        fun bindItem(article: Article) {
            articleTitle.text = article.name
            articleBody.text = article.shortDescription
            article.picture?.let{
                articleImage.load(it)
            }
        }

    }


}