package victor_gonzalez_ollervidez.kinedu.utils.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.find
import victor_gonzalez_ollervidez.kinedu.R
import victor_gonzalez_ollervidez.kinedu.utils.AspectRatioImageView
import victor_gonzalez_ollervidez.kinedu.utils.Interfaces
import victor_gonzalez_ollervidez.kinedu.utils.load
import victor_gonzalez_ollervidez.kinedu.utils.pojos.Activities.Activity

class ActivitiesListAdapter: RecyclerView.Adapter<ActivitiesListAdapter.ActivityHolder>() {

    val TAG = javaClass.simpleName

    lateinit var onClickListener: Interfaces.OnClickListener<Activity>

    private val activitiesList = ArrayList<Activity>()

    fun bindList(activities: List<Activity>){
        this.activitiesList.addAll(activities)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActivityHolder {
        return ActivityHolder(
            LayoutInflater.from(
                parent.context
            ).inflate(R.layout.activity_list_item, parent, false)
        )
    }

    override fun getItemCount() = this.activitiesList.size

    override fun onBindViewHolder(holder: ActivityHolder, position: Int) {
        holder.bindItem(
            activitiesList[position]
        )
    }


    inner class ActivityHolder (itemView: View) : RecyclerView.ViewHolder(itemView){

        private val activityImage = itemView.find<AspectRatioImageView>(R.id.activity_image)
        private val activityTitle = itemView.find<TextView>(R.id.activity_title)
        private val activityBody = itemView.find<TextView>(R.id.activity_body)

        init {
            itemView.setOnClickListener {
                if(::onClickListener.isInitialized){
                    onClickListener.onClick(
                        activitiesList[adapterPosition],
                        adapterPosition,
                        itemView
                    )
                }
            }
        }

        fun bindItem(activity: Activity) {
            activityTitle.text = activity.name
            activityBody.text = activity.purpose
            activity.thumbnail?.let{
                activityImage.load(it)
            }
        }

    }


}