package victor_gonzalez_ollervidez.kinedu.utils.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerAdapter (fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager) {

    private val fragments = ArrayList<Fragment>()

    fun addFragments(vararg fragments: Fragment){
        this.fragments.addAll(fragments)
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        return this.fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Activities"
            1 -> "Articles"
            else -> "-"
        }
    }
}