package victor_gonzalez_ollervidez.kinedu.ui.details

import victor_gonzalez_ollervidez.kinedu.utils.BaseViewModel

class DetailViewModel: BaseViewModel(){


    val articleDetail = repository.articleDetail
    val activityDetail = repository.activityDetail


    fun loadArticle(articleId: Int){
        repository.loadArticle(articleId)
    }


    fun loadActivity(activityId: Int){
        repository.loadActivity(activityId)
    }

}