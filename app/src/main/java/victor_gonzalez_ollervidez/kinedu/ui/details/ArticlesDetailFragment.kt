package victor_gonzalez_ollervidez.kinedu.ui.details


import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.loading.*
import kotlinx.android.synthetic.main.loading_error.*
import victor_gonzalez_ollervidez.kinedu.R
import victor_gonzalez_ollervidez.kinedu.utils.DETAIL_ID
import victor_gonzalez_ollervidez.kinedu.utils.load
import victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail.Article

/**
 * A simple [Fragment] subclass.
 */
class ArticlesDetailFragment : Fragment() {

    companion object {
        fun newInstance(id: Int): ArticlesDetailFragment{
            val instance = ArticlesDetailFragment()
            val args = Bundle()
            args.putInt(DETAIL_ID, id)
            instance.arguments = args
            return instance
        }
    }

    private lateinit var viewModel: DetailViewModel
    var detailId : Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(arguments != null){
            detailId = arguments!!.getInt(DETAIL_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)
        viewModel.loadArticle(detailId)
        viewModel.articleDetail.observe(this, Observer {
            articleDetail ->
            if(loading.visibility == View.VISIBLE){
                loading.visibility = View.GONE
            }
            if(articleDetail != null){
                displayData(articleDetail)
                loading_error.visibility = View.GONE
                detail.visibility = View.VISIBLE
            }else{
                loading_error.visibility = View.VISIBLE
                detail.visibility = View.GONE
            }
        })

        reload.setOnClickListener {
            viewModel.loadArticle(detailId)
        }
    }

    private fun displayData(articleDetail: Article) {
        detail_title.text = articleDetail.title
        articleDetail.picture?.let{
            detail_image.load(it)
        }
        detail_body.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(articleDetail.body, HtmlCompat.FROM_HTML_MODE_LEGACY)
        }else{
            Html.fromHtml(articleDetail.body)
        }
    }

}
