package victor_gonzalez_ollervidez.kinedu.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activities_fragment.*
import kotlinx.android.synthetic.main.loading.*
import kotlinx.android.synthetic.main.loading_error.*
import victor_gonzalez_ollervidez.kinedu.R
import victor_gonzalez_ollervidez.kinedu.ui.DetailActivity
import victor_gonzalez_ollervidez.kinedu.utils.*
import victor_gonzalez_ollervidez.kinedu.utils.adapters.ActivitiesListAdapter


class ActivitiesFragment : Fragment() {

    companion object {
        fun newInstance() = ActivitiesFragment()
    }

    private lateinit var viewModel: ActivitiesViewModel
    private val activitiesListAdapter = ActivitiesListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activities_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ActivitiesViewModel::class.java)

        viewModel.activitiesList.observe(this,  Observer {
            list ->
            if(loading.visibility == View.VISIBLE){
                loading.visibility = View.GONE
            }
            loading_error.visibility = if(list != null){
                activitiesListAdapter.bindList(list)
                View.GONE
            }else{
                View.VISIBLE
            }
        })

        setupUI()

    }


    private fun setupUI() {
        val layoutManager = LinearLayoutManager(context)
        activities_list.layoutManager = layoutManager
        activities_list.adapter = activitiesListAdapter

        activitiesListAdapter.onClickListener = Interfaces.OnClickListener { activity, pos, v ->
            goToDetail(activity.id)
        }

        reload.setOnClickListener {
            viewModel.reloadList()
        }

        activities_list.addOnScrollListener(object : EndlessRecyclerViewScroll(layoutManager){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                viewModel.loadPage(page)
            }
        })

    }

    private fun goToDetail(activityId: Int) {
        context?.let{
            val detailIntent = Intent(it, DetailActivity::class.java)
            detailIntent.putExtra(
                DETAIL_TYPE,
                DETAIL_ACTIVITY
            )
            detailIntent.putExtra(ARTICLE_ID, activityId)
            it.startActivity(detailIntent)
        }
    }

}
