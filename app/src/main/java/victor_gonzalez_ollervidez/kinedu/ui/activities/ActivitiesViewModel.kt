package victor_gonzalez_ollervidez.kinedu.ui.activities

import victor_gonzalez_ollervidez.kinedu.utils.BaseViewModel

class ActivitiesViewModel : BaseViewModel() {

    val activitiesList = repository.activitiesData

    init{
        repository.loadActivities()
    }

    fun reloadList(){
        repository.loadActivities()
    }

    fun loadPage(page: Int) {
        repository.loadActivities(page)
    }

}
