package victor_gonzalez_ollervidez.kinedu.ui.articles

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.articles_fragment.*
import kotlinx.android.synthetic.main.loading.*
import kotlinx.android.synthetic.main.loading_error.*
import victor_gonzalez_ollervidez.kinedu.R
import victor_gonzalez_ollervidez.kinedu.ui.DetailActivity
import victor_gonzalez_ollervidez.kinedu.utils.*
import victor_gonzalez_ollervidez.kinedu.utils.adapters.ArticlesListAdapter


class ArticlesFragment : Fragment() {

    companion object {
        fun newInstance() = ArticlesFragment()
    }

    private lateinit var viewModel: ArticlesViewModel

    private val articlesAdapter =  ArticlesListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.articles_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ArticlesViewModel::class.java)

        viewModel.articlesList.observe(this, Observer {
            list ->
            if(loading.visibility == View.VISIBLE){
                loading.visibility = View.GONE
            }
            if(list != null){
                articlesAdapter.bindList(list)
                loading_error.visibility = View.GONE
            }else{
                loading_error.visibility = View.VISIBLE
            }
        })

        bindUI()

    }

    private fun bindUI() {
        val layoutManager = LinearLayoutManager(context)
        articles_list.adapter = articlesAdapter
        articles_list.layoutManager = layoutManager

        articlesAdapter.onClickListener = Interfaces.OnClickListener { article, pos, view ->
            goToDetail(article.id)
        }

        articles_list.addOnScrollListener(object : EndlessRecyclerViewScroll(layoutManager){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                viewModel.loadPage(page)
            }
        })

        reload.setOnClickListener {
            viewModel.reloadList()
        }
    }

    private fun goToDetail(articleId: Int) {
        context?.let{
            val detailIntent = Intent(it, DetailActivity::class.java)
            detailIntent.putExtra(
                DETAIL_TYPE,
                DETAIL_ARTICLE
            )
            detailIntent.putExtra(ARTICLE_ID, articleId)
            it.startActivity(detailIntent)
        }
    }

}
