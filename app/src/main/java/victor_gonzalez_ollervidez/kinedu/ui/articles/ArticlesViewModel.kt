package victor_gonzalez_ollervidez.kinedu.ui.articles

import victor_gonzalez_ollervidez.kinedu.utils.BaseViewModel

class ArticlesViewModel : BaseViewModel() {

    val articlesList = repository.articlesData

    init{
        repository.loadArticles()
    }

    fun reloadList() {
        repository.loadArticles()
    }

    fun loadPage(page: Int){
        repository.loadArticles(page)
    }
}
