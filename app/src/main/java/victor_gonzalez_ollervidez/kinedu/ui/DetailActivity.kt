package victor_gonzalez_ollervidez.kinedu.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_article_detail.*
import victor_gonzalez_ollervidez.kinedu.R
import victor_gonzalez_ollervidez.kinedu.ui.details.ActivityDetailFragment
import victor_gonzalez_ollervidez.kinedu.ui.details.ArticlesDetailFragment
import victor_gonzalez_ollervidez.kinedu.utils.ARTICLE_ID
import victor_gonzalez_ollervidez.kinedu.utils.DETAIL_ACTIVITY
import victor_gonzalez_ollervidez.kinedu.utils.DETAIL_ARTICLE
import victor_gonzalez_ollervidez.kinedu.utils.DETAIL_TYPE


class DetailActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_detail)

        setupUI()

        if(savedInstanceState == null){

            val detailType = intent.getIntExtra(
                DETAIL_TYPE,
                DETAIL_ARTICLE
            )
            val id = intent.getIntExtra(ARTICLE_ID, -1)

            if(detailType == DETAIL_ARTICLE){

                val articlesDetailFragment = ArticlesDetailFragment.newInstance(id)
                supportFragmentManager
                    .beginTransaction()
                    .add(R.id.container, articlesDetailFragment)
                    .commit()

            }else if(detailType == DETAIL_ACTIVITY){
                val activityDetailFragment =
                    ActivityDetailFragment.newInstance(id)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, activityDetailFragment)
                    .commit()
            }

        }

    }

    private fun setupUI() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}
