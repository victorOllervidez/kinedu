package victor_gonzalez_ollervidez.kinedu.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import victor_gonzalez_ollervidez.kinedu.R
import victor_gonzalez_ollervidez.kinedu.ui.activities.ActivitiesFragment
import victor_gonzalez_ollervidez.kinedu.ui.articles.ArticlesFragment
import victor_gonzalez_ollervidez.kinedu.utils.adapters.ViewPagerAdapter


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)

        viewPagerAdapter.addFragments(
            ActivitiesFragment.newInstance(),
            ArticlesFragment.newInstance()
        )

        pager.adapter = viewPagerAdapter
        tab_layout.setupWithViewPager(pager)
    }

}
