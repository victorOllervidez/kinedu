package victor_gonzalez_ollervidez.kinedu.networking

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import victor_gonzalez_ollervidez.kinedu.BuildConfig
import victor_gonzalez_ollervidez.kinedu.utils.pojos.Activities.ActivitiesResponse
import victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail.ActivityDetailResponse
import victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail.ArticleDetailResponse
import victor_gonzalez_ollervidez.kinedu.utils.pojos.Articles.ArticlesResponse


interface ApiKinedu {

    @GET("api/v3/catalogue/articles")
    fun getArticles(
        @Query("skill_id") skillId: Long,
        @Query("baby_id") babyId: Long,
        @Query("page") page: Int
    ): Call<ArticlesResponse>

    @GET("api/v3/catalogue/activities")
    fun getActivities(
        @Query("skill_id") skillId: Long,
        @Query("baby_id") babyId: Long,
        @Query("page") page: Int
    ): Call<ActivitiesResponse>


    @GET("api/v3/articles/{article_id}")
    fun getArticleDetail(
        @Path("article_id") articleId: Int
    ): Call<ArticleDetailResponse>

    @GET("api/v3/activities/{activity_id}")
    fun getActivityDetail(
        @Path("activity_id") activityId: Int
    ): Call<ActivityDetailResponse>


    companion object Factory {

        @Volatile
        private var retrofit : Retrofit? = null

        private val BASE_URL: String = BuildConfig.BASE_URL

        @Synchronized
        fun getInstance(): ApiKinedu {
            retrofit ?: synchronized(this) {
                retrofit = buildRetrofit()
            }

            return retrofit!!.create(ApiKinedu::class.java)
        }

        private var client = OkHttpClient.Builder().addInterceptor { chain ->
            val newRequest = chain.request().newBuilder()
                .addHeader("Authorization", BuildConfig.TOKEN)
                .addHeader("Accept", "application/json")
                .build()
            chain.proceed(newRequest)
        }.build()

        private fun buildRetrofit() = retrofit2.Retrofit.Builder()
            .client(client)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}