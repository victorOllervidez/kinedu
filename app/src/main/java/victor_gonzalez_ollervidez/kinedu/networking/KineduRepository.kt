package victor_gonzalez_ollervidez.kinedu.networking

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import victor_gonzalez_ollervidez.kinedu.utils.pojos.Activities.ActivitiesResponse
import victor_gonzalez_ollervidez.kinedu.utils.pojos.Activities.Activity
import victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail.ActivityDetail
import victor_gonzalez_ollervidez.kinedu.utils.pojos.ActivityDetail.ActivityDetailResponse
import victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail.ArticleDetailResponse
import victor_gonzalez_ollervidez.kinedu.utils.pojos.Articles.Article
import victor_gonzalez_ollervidez.kinedu.utils.pojos.Articles.ArticlesResponse
import victor_gonzalez_ollervidez.kinedu.utils.pojos.ArticleDetail.Article as ArticleDetail
class KineduRepository {

    val TAG = javaClass.simpleName

    private val api = ApiKinedu.getInstance()
    private val BABY_ID = 2064732L // <-- just for this exam

    val articlesData = MutableLiveData<List<Article>>()
    val activitiesData = MutableLiveData<List<Activity>>()

    val articleDetail = MutableLiveData<ArticleDetail>()
    val activityDetail = MutableLiveData<ActivityDetail>()

    fun loadArticles(page: Int = 1){

        api.getArticles(
            5,
            BABY_ID,
            page
        ).enqueue(object: Callback<ArticlesResponse> {
            override fun onFailure(call: Call<ArticlesResponse>, t: Throwable) {
                articlesData.postValue(null)
            }

            override fun onResponse(
                call: Call<ArticlesResponse>,
                response: Response<ArticlesResponse>
            ) {
                if(response.isSuccessful){
                    val data = response.body()
                    data?.let{
                        articlesData.postValue(it.data?.articles)
                    }
                }
            }
        })

    }


    fun loadActivities(page: Int = 1){

        api.getActivities(
            5,
            BABY_ID,
            page
        ).enqueue(object: Callback<ActivitiesResponse> {
            override fun onFailure(call: Call<ActivitiesResponse>, t: Throwable) {
                activitiesData.postValue(null)
            }

            override fun onResponse(
                call: Call<ActivitiesResponse>,
                response: Response<ActivitiesResponse>
            ) {
                if(response.isSuccessful){

                    val data = response.body()
                    data?.let{
                        activitiesData.postValue(it.data?.activities)
                    }
                }
            }
        })

    }

    fun loadArticle(articleId: Int) {
        api.getArticleDetail(
            articleId
        ).enqueue(object: Callback<ArticleDetailResponse> {
            override fun onFailure(call: Call<ArticleDetailResponse>, t: Throwable) {
                articleDetail.postValue(null)
            }

            override fun onResponse(
                call: Call<ArticleDetailResponse>,
                response: Response<ArticleDetailResponse>
            ) {
                if(response.isSuccessful){

                    val data = response.body()
                    data?.let{
                        articleDetail.postValue(it.data?.article)
                    }
                }
            }
        })
    }

    fun loadActivity(activityId: Int){
        api.getActivityDetail(
            activityId
        ).enqueue(object: Callback<ActivityDetailResponse> {
            override fun onFailure(call: Call<ActivityDetailResponse>, t: Throwable) {
                activityDetail.postValue(null)
            }

            override fun onResponse(
                call: Call<ActivityDetailResponse>,
                response: Response<ActivityDetailResponse>
            ) {
                if(response.isSuccessful){

                    val data = response.body()
                    data?.let{
                        activityDetail.postValue(it.data?.activity)
                    }
                }
            }
        })
    }


}